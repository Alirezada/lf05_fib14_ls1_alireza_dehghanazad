import java.util.Scanner;
public class Temperaturumrechnung {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/*Aufgabe 4: Temperaturumrechnung 
Erstellen und testen Sie ein Java-Programm, das dem dargestellten Beispiel entsprechend 
Temperaturwerte von Celsius nach Fahrenheit umrechnet.
Bitte den Startwert in Celsius eingeben: -10
Bitte den Endwert in Celsius eingeben: 20
Bitte die Schrittweite in Grad Celsius eingeben: 5
-10,00�C 14,00�F
-5,00�C 23,00�F
0,00�C 32,00�F
5,00�C 41,00�F
10,00�C 50,00�F
15,00�C 59,00�F
20,00�C 68,00�F
*/
		Scanner lucky=new Scanner(System.in);
		System.out.println("Bitte den Startwert in Celsius eingeben:");
		double startwert=lucky.nextDouble();
		System.out.println("Bitte den Endwert in Celsius eingeben:");
		double endwert=lucky.nextDouble();
		System.out.println("Bitte die Schrittweite in Grad Celsius eingeben: 5");
		double schrittweite=lucky.nextDouble();
		int count=0;
		double fahrenheit=0;
		fahrenheit=(startwert*9/5)+32;
		System.out.printf("%.2f�C %.2f�F\n",startwert,fahrenheit);
		while (count<schrittweite) {
				startwert+=schrittweite;
				fahrenheit=(startwert*9/5)+32;
				System.out.printf("%.2f�C %.2f�F\n",startwert,fahrenheit);
				count++;
		}
		fahrenheit=(endwert*9/5)+32;
		System.out.printf("%.2f�C %.2f�F\n",endwert,fahrenheit);

	}

}
