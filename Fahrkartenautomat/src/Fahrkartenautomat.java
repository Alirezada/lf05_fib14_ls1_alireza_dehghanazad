﻿import java.util.Scanner;

class Fahrkartenautomat {
	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);
		double zuZahlenderBetrag;
		double eingezahlterGesamtbetrag;
		// double eingeworfeneMünze;
		// double rückgabebetrag;
		// double Ticketpreis;
		/*Wählen Sie ihre Wunschfahrkarte für Berlin AB aus:
		  Einzelfahrschein Regeltarif AB [2,90 EUR] (1)
		  Tageskarte Regeltarif AB [8,60 EUR] (2)
		  Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)
		  */
		while(true) {
		zuZahlenderBetrag = fahrkartenbestellungErfassen();
		System.out.printf("Zu zahlender Betrag (Euro): %.2f \n" ,zuZahlenderBetrag);
		// Geldeinwurf
		// -----------
		eingezahlterGesamtbetrag = 0.0;
		double rückgabebetrag = fahrkartenBezahlen(eingezahlterGesamtbetrag, zuZahlenderBetrag);
		// Fahrscheinausgabe
		// -----------------
		fahrkartenAusgeben();
		rueckgeldAusgeben(rückgabebetrag);
		}
	}

	static double fahrkartenbestellungErfassen() {
		Scanner tastatur = new Scanner(System.in);
		System.out.println("Wählen Sie ihre Wunschfahrkarte für Berlin AB aus:\r\n"
				+ "  Einzelfahrschein Regeltarif AB [2,90 EUR] (1)\r\n"
				+ "  Tageskarte Regeltarif AB [8,60 EUR] (2)\r\n"
				+ "  Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)");
		int wunschfahrkarteNum=tastatur.nextInt();
		double Ticketpreis=0;
		switch (wunschfahrkarteNum) {
		
		case 1: Ticketpreis=2.90; break;
		case 2: Ticketpreis=8.60;break;
		case 3: Ticketpreis=23.50;break;
		case 4: System.out.println("falsche Eingabe");
		default :break;
		
		}
		//System.out.print("Ticketpreis (Euro): ");
		//double Ticketpreis = tastatur.nextDouble();
		System.out.print("Anzahl der Tickets: ");
		int anzahlDerTickets = tastatur.nextInt();
		//default
		final int standardwert=1;
		if (anzahlDerTickets>=1&&anzahlDerTickets<10) 
		{
			return Ticketpreis * anzahlDerTickets;
		}
		else {
			System.out.println("Ungültiger Wert wurde eingegeben, der Standardwert für die Anzahl der Tickets verwendet.");
			anzahlDerTickets=standardwert;
			return Ticketpreis * anzahlDerTickets;
		}
	}
	private static double fahrkartenBezahlen(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
		double r = 0;
		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
			Scanner tastatur = new Scanner(System.in);
			double eingeworfeneMünze = tastatur.nextDouble();
			if (eingeworfeneMünze > 0.5 && eingeworfeneMünze <= 2) {
				eingezahlterGesamtbetrag += eingeworfeneMünze;
				System.out.printf("Noch zu zahlen: %.2f \n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
				r = eingezahlterGesamtbetrag - zuZahlenderBetrag;

			} else {

				System.out.println("ungültige Eingabe");
			}
		}
		return r;
	}
	static void fahrkartenAusgeben()

	{
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			warte(250);

		}
		System.out.println("\n\n");
	}

	static void warte(int millisekunde) {

		try {
			Thread.sleep(millisekunde);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	static void muenzeAusgeben(int betrag, String einheit) {

		System.out.println(betrag + " " + einheit);

	}

	static void rueckgeldAusgeben(double rückgabebetrag) {
		if (rückgabebetrag > 0.0) {
			System.out.printf("Der Rückgabebetrag in Höhe von %.2f Euro \n", rückgabebetrag);
			System.out.println("wird in folgenden Münzen ausgezahlt:");

			while (rückgabebetrag >= 2.0) // 2 EURO-Münzen
			{
				// System.out.println(betrag + "EURO");
				muenzeAusgeben(2, "EURO");
				rückgabebetrag -= 2.0;
			}
			while (rückgabebetrag >= 1.0) // 1 EURO-Münzen
			{

				muenzeAusgeben(1, "EURO");
				rückgabebetrag -= 1.0;
			}
			while (rückgabebetrag >= 0.5) // 50 CENT-Münzen
			{
				muenzeAusgeben(50, "CENT");
				rückgabebetrag -= 0.5;
			}
			while (rückgabebetrag >= 0.2) // 20 CENT-Münzen
			{
				muenzeAusgeben(20, "CENT");
				rückgabebetrag -= 0.2;
			}
			while (rückgabebetrag >= 0.1) // 10 CENT-Münzen
			{
				muenzeAusgeben(10, "CENT");
				rückgabebetrag -= 0.1;
			}
			// rückgabebetrag kann auch 0.04999999999993 sein
			while (rückgabebetrag >= 0.05 || rückgabebetrag > 0.001)// 5 CENT-Münzen
			{
				muenzeAusgeben(5, "CENT");
				rückgabebetrag -= 0.05;
			}
		}
		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir wünschen Ihnen eine gute Fahrt.\n");
		System.out.println("*********************");
		System.out.println("Fahrkartenautomat\n");
		
	}
}
